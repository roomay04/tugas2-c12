from django import forms


class ReplyForm(forms.Form):
    sender = forms.CharField(max_length=60)
    message = forms.CharField(max_length=250)
