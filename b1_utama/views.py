from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from .models import User
from b3_forum.models import Forum
from b4_tanggapan.views import paginate


# Create your views here.
response={}
def index(request):
    html = 'b1_utama/b1_utama.html'
    forum_list = Forum.objects.all()

    page = request.GET.get('page', 1)
    # membuat paginasi
    paginate_data = paginate(page, forum_list)
    forum = paginate_data['forum']
    page_range = paginate_data['page_range']

    response = {'author':'Nixi Sendya',"forum_list": forum, "page_range": page_range}

    return render(request,html,response)